import * as React from "react";
import styled from "styled-components";
import { Stepper, Step, StepLabel, Typography, Button, TextField, Grid } from "material-ui";
import { CssGrid, CssGridItem, ButtonsArea, EsteamationPaper } from "../util/layout";
import { Redirect } from "react-router";

interface State {
    teamName: string;
    submitted: boolean;
}

export default class SelectTeam extends React.Component<any, State> {
    state: State = {
        teamName: "",
        submitted: false
    };

    teamNameChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newTeamName = e.target.value;
        this.setState({
            teamName: newTeamName
        });
    }

    onSubmit = (e: React.SyntheticEvent<any>) => {
        e.preventDefault();
        this.setState({
            submitted: true,
        });
    }

    render() {
        if (this.state.submitted) {
            return <Redirect push to={`/${this.state.teamName}`} />;
        }

        return (
            <form onSubmit={this.onSubmit}>
                <CssGrid>
                    <CssGridItem justify="center">
                        <Typography type="headline">Enter your team name:</Typography>
                        <TextField
                            autoFocus
                            fullWidth
                            label="Team Name"
                            required={true}
                            onChange={this.teamNameChanged}
                        />
                    </CssGridItem>
                    <ButtonsArea justify="center" >
                        <Button type="submit" raised={true} color="primary">OK</Button>
                    </ButtonsArea>
                </CssGrid>
            </form>
        );
    }
}
