import EstimateInputPanel from "./EstimateInputPanel";
import ListItemSecondaryAction from "material-ui/List/ListItemSecondaryAction";
import ListItemText from "material-ui/List/ListItemText";
import { Estimation, Member, Team, Role } from "../Entities";
import Typography from "material-ui/Typography";
import * as React from "react";
import {
    ExpansionPanel,
    ExpansionPanelSummary,
    ExpansionPanelDetails,
    ExpansionPanelActions,
    IconButton,
    Button,
    Chip,
    Avatar
} from "material-ui";
import ExpandMore from "material-ui-icons/ExpandMore";
import { canEstimate, hasEstimated, getEstimationSummary, checkEstimationFinished } from "./estimationUtils";
import PriorityHigh from "material-ui-icons/PriorityHigh";
import DeleteIcon from "material-ui-icons/Delete";
import List from "material-ui/List/List";
import ListItem from "material-ui/List/ListItem";
import styled from "styled-components";
import EstimatingMemberChip from "./EstimatingMemberChip";
import EstimationResults from "./EstimationResults";

const DetailsBlock = styled.div`
    word-break: break-word;
    width: 100%;
`;
const EstimatorsBlock = styled.div`
    margin-top: 24px;
    display: flex;
    flex-wrap: wrap;
`;

interface Props {
    estimation: Estimation;
    user: Member;
    team: Team;
    onEstimationValueSelected: (estimation: Estimation, value: number) => void;
    onDeleteEstimationRequested: (estimation: Estimation) => void;
}

export default class EstimationEntry extends React.Component<Props> {

    private getEstimatedValue(m: Member) {
        const v = this.props.estimation.values[m.name];
        return typeof v === "undefined" ? null : v;
    }

    render() {
        const userCanEstimate = canEstimate(this.props.team, this.props.user, this.props.estimation);
        const userHasEstimated = hasEstimated(this.props.team, this.props.user, this.props.estimation);
        const showAsPending = userCanEstimate && !userHasEstimated;
        const isFinished = checkEstimationFinished(this.props.team, this.props.estimation);
        const summary = getEstimationSummary(this.props.team, this.props.estimation);
        const estimatingMembers = this.props.team.members.filter(m => m.role === Role.Estimator);

        return (
            <ExpansionPanel>

                <ExpansionPanelSummary expandIcon={<ExpandMore />}>
                    <Typography style={{flexGrow: 2, fontWeight: showAsPending ? "bold" : "normal"}}>
                        {this.props.estimation.name} {showAsPending && "*"}
                    </Typography>
                    <Typography type="caption">{summary}</Typography>

                </ExpansionPanelSummary>

                <ExpansionPanelDetails style={{paddingBottom: 0}}>
                    <DetailsBlock>
                        {userCanEstimate && (
                            <div>
                                <Typography type="caption">Your estimate:</Typography>
                                <EstimateInputPanel
                                    onEstimationSelected={v =>
                                        this.props.onEstimationValueSelected(this.props.estimation, v)}
                                />
                            </div>)
                        }
                        {isFinished && (
                            <EstimationResults estimation={this.props.estimation} />
                        )}

                        <EstimatorsBlock>
                            {estimatingMembers.map((m, i) =>
                                <EstimatingMemberChip
                                    key={m.name}
                                    member={m}
                                    revealValue={isFinished || m.name === this.props.user.name}
                                    estimatedValue={this.getEstimatedValue(m)}
                                />)
                            }
                        </EstimatorsBlock>

                    </DetailsBlock>
                </ExpansionPanelDetails>

                <ExpansionPanelActions>
                    <Button
                        raised
                        color="primary"
                        onClick={() => this.props.onDeleteEstimationRequested(this.props.estimation)}>
                        Delete
                    </Button>
                </ExpansionPanelActions>

            </ExpansionPanel>
        );
    }
}
