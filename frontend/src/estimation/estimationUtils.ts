import { Estimation, Member, Role, Team } from "../Entities";

export function canEstimate(team: Team, member: Member, estimation: Estimation) {
    return !checkEstimationFinished(team, estimation) && member.role === Role.Estimator; // &&
        // !Object.keys(estimation.values).find(name => name === member.name);
}

export function hasEstimated(team: Team, member: Member, estimation: Estimation) {
    return !!Object.keys(estimation.values).find(name => name === member.name);
}

export function checkEstimationFinished(team: Team, estimation: Estimation) {
    return estimation.isFinished ||
        team.members
            .filter(m => m.role === Role.Estimator)
            .every(m => typeof estimation.values[m.name] !== "undefined");
}

export function getEstimationSummary(team: Team, estimation: Estimation) {
    let summary = "";
    if (checkEstimationFinished(team, estimation)) {
        summary = "completed";
    } else {
        // estimates remaining
        const estimatesDoneCount = Object.keys(estimation.values).length;
        const estimatesRequiredCount = team.members.filter(m => m.role === Role.Estimator).length;
        summary = `${estimatesRequiredCount - estimatesDoneCount} / ${estimatesRequiredCount} estimates remaining`;
    }
    return summary;
}
