import * as React from "react";
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button } from "material-ui";

interface Props {
    isOpen: boolean;
    onClose: (confirmed: boolean) => void;
}

export default class DeleteAllEstimationsDialog extends React.Component<Props> {

    onCancel = () => {
        this.props.onClose(false);
    }

    onConfirm = () => {
        this.props.onClose(true);
    }

    render() {
        return (
            <Dialog
                open={this.props.isOpen}
                onClose={this.onCancel}
            >
            <DialogTitle>
              Confirmation
            </DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Do you really want to delete all estimations for this team?
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={this.onCancel} color="primary">
                    Cancel
                </Button>
                <Button onClick={this.onConfirm} color="primary" autoFocus>
                    Delete
                </Button>
            </DialogActions>
        </Dialog>
        );
    }
}
