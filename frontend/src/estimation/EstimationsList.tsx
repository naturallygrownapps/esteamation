import * as React from "react";
import Typography from "material-ui/Typography/Typography";
import { Team, Estimation, Member, Role } from "../Entities";
import { CssGridItem, CssGrid } from "../util/layout";
import { IconButton } from "material-ui";
import List, {
    ListItem,
    ListItemSecondaryAction,
    ListItemText,
  } from "material-ui/List";
import DeleteIcon from "material-ui-icons/Delete";
import PriorityHigh from "material-ui-icons/PriorityHigh";
import styled from "styled-components";
import EstimationEntry from "./EstimationEntry";

interface Props {
    estimations: Estimation[];
    onEstimationValueSelected: (estimation: Estimation, value: number) => void;
    onDeleteEstimationRequested: (estimation: Estimation) => void;
    user: Member;
    team: Team;
}

class EstimationsList extends React.Component<Props> {

    render() {
        const estimationItems = this.props.estimations.map(e => (
            <EstimationEntry
                key={e.name}
                estimation={e}
                team={this.props.team}
                user={this.props.user}
                onEstimationValueSelected={this.props.onEstimationValueSelected}
                onDeleteEstimationRequested={this.props.onDeleteEstimationRequested}
             />
        ));

        return (
            <React.Fragment>
                <List>
                    {estimationItems}
                </List>
            </React.Fragment>
        );
    }
}
export default EstimationsList;
