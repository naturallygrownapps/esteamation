import { Estimation } from "../Entities";
import * as React from "react";
import styled from "styled-components";
import ArrowUp from "material-ui-icons/ArrowDropUp";
import ArrowDown from "material-ui-icons/ArrowDropDown";
import Avatar from "material-ui/Avatar/Avatar";

const StatsBlock = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
`;
const StatsItem = styled.span`

`;
const StatsValue = styled.span`
    display: inline-block;
    transform: translateY(-4px);
`;
const styles = {
    avgAvatar: {
      backgroundColor: "green",
    }
};

interface Props {
    estimation: Estimation;
}

export default class EstimationResults extends React.Component<Props> {
    render() {
        const values = Object.keys(this.props.estimation.values).map(k => this.props.estimation.values[k]);
        const highest = values.length > 0 ? Math.max(...values) : 0;
        const lowest = values.length > 0 ? Math.min(...values) : 0;
        let avg = values.reduce((prev, cur) => prev + cur, 0) / values.length;
        avg = Math.round(avg * 10) / 10;
        return (
            <StatsBlock>
                <StatsItem>
                    <ArrowDown /> <StatsValue>{lowest}</StatsValue>
                </StatsItem>
                <Avatar style={styles.avgAvatar}>
                    {avg}
                </Avatar>
                <StatsItem>
                    <ArrowUp /> <StatsValue>{highest}</StatsValue>
                </StatsItem>
            </StatsBlock>
        );
    }
}
