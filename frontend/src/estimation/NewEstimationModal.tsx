import * as React from "react";
import { Typography, Button, TextField, Dialog, DialogTitle, DialogContent, DialogActions } from "material-ui";
import { Estimation } from "../Entities";
import { OffsetButton } from "../util/layout";
import styled from "styled-components";

interface Props {
    isOpen: boolean;
    onCloseRequested: (result: Estimation|null) => void;
}

interface State {
    name: string;
}

const NameInput = styled(TextField as any)`
    margin-top: 16px !important;
`;

export default class NewEstimationModal extends React.Component<Props, State> {
    state: State = {
        name: ""
    };

    onSubmit = (e: React.SyntheticEvent<any>) => {
        e.preventDefault();
        if (this.state.name) {
            const estimation = new Estimation(this.state.name);
            this.props.onCloseRequested(estimation);
            this.setState({
                name: ""
            });
        }
    }

    nameChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            name: e.target.value
        });
    }

    inputKeyDown = (e: React.KeyboardEvent<HTMLDivElement>) => {
        if (e.key === "Enter") {
            this.onSubmit(e);
        }
    }

    render() {
        return (
            <Dialog
                open={this.props.isOpen}
                disableBackdropClick>
                <DialogTitle>
                    Add new estimation
                </DialogTitle>

                <DialogContent>
                    <Typography type="subheading">
                        Type a name for the estimation:
                    </Typography>
                    <NameInput
                        autoFocus
                        fullWidth
                        label="Estimation Name"
                        required
                        onChange={this.nameChanged}
                        onKeyDown={this.inputKeyDown} />
                </DialogContent>

                <DialogActions>
                    <Button onClick={() => this.props.onCloseRequested(null)} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={this.onSubmit} raised color="primary">
                        Add
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}
