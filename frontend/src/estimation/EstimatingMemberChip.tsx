import styled from "styled-components";
import { Member } from "../Entities";
import * as React from "react";
import { Chip, Avatar } from "material-ui";
import HourGlassIcon from "material-ui-icons/HourglassEmpty";
import CheckIcon from "material-ui-icons/Check";

const MemberChip = styled(Chip as any)`
    margin-right: 5px;
    margin-bottom: 5px;
`;

const styles = {
    checkedAvatar: {
      backgroundColor: "darkseagreen",
      fontSize: "0.9rem"
    }
};

interface Props {
    member: Member;
    revealValue: boolean;
    estimatedValue: number|null;
}

export default class EstimatingMemberChip extends React.Component<Props> {
    render() {
        const m = this.props.member;
        return (
            <MemberChip
                key={m.name}
                label={m.name}
                avatar={this.props.estimatedValue !== null ?
                    <Avatar style={styles.checkedAvatar}>
                        {this.props.revealValue ? this.props.estimatedValue : <CheckIcon />}
                    </Avatar> :
                    <Avatar>
                        <HourGlassIcon />
                    </Avatar>}
            />
        );
    }
}
