import * as React from "react";
import { Button } from "material-ui";
import styled from "styled-components";

const EstimatesPanel = styled.div`
    display: float;
`;

const EstimatePresetButton = styled(Button as any)`
    margin: 5px 5px;
`;

interface Props {
    onEstimationSelected: (value: number) => void;
}

export default class EstimateInputPanel extends React.Component<Props> {
    readonly presets = [ 0.25, 0.5, 1, 2, 3, 4, 6, 8, 10, 12, 16, 20, 24, 32, 40];

    render() {
        return (
            <EstimatesPanel>
                {this.presets.map(p => (
                    <EstimatePresetButton
                        key={p}
                        fab
                        mini
                        color="primary"
                        onClick={() => this.props.onEstimationSelected(p)}
                    >
                        {p}
                    </EstimatePresetButton>
                ))}
            </EstimatesPanel>
        );
    }
}
