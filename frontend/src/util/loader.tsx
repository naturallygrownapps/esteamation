import * as React from "react";
import Backdrop from "material-ui/Modal/Backdrop";
import CircularProgress from "material-ui/Progress/CircularProgress";
import zIndex from "material-ui/styles/zIndex";
import { CssGrid, CssGridItem } from "./layout";
import { CSSProperties } from "react";

interface WithLoaderState {
    awaitedPromise?: Promise<any>;
}

export interface WithLoaderProps {
    loadWhilePromise: <T = any>(p: Promise<T>) => Promise<T>;
}

export function withLoader<TOriginalProps extends {}>(
    Component: React.ComponentType<TOriginalProps & WithLoaderProps>
) {

    type Props = TOriginalProps & WithLoaderProps;
    return class extends React.Component<Props, WithLoaderState> {
        state = {
            awaitedPromise: undefined,
        };

        loadWhilePromise = async (promise: Promise<any>) => {
            this.setState({
                awaitedPromise: promise
            });
            try {
                return await promise;
            }
            finally {
                this.setState({
                    awaitedPromise: undefined
                });
            }
        }

        render() {
            const showLoading = !!this.state.awaitedPromise;
            const outerStyle: CSSProperties = {
                position: "fixed",
                top: 0, right: 0, bottom: 0, left: 0,
                zIndex: showLoading ? 1000 : -1,
                display: showLoading ? "block" : "none"
            };
            const progressStyle: CSSProperties = {
                transform: "translate(-50%, -50%)",
                position: "relative",
                top: "50%",
                left: "50%"
            };
            return (
                <React.Fragment>
                    <div style={outerStyle}>
                        <Backdrop
                            open={showLoading}
                            invisible={!showLoading}
                        />
                        <CircularProgress style={progressStyle} />
                    </div>
                    <Component {...this.props} loadWhilePromise={this.loadWhilePromise} />
                </React.Fragment>
            );
        }
    };
}
