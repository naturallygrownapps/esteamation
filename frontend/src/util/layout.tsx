import * as React from "react";
import styled from "styled-components";
import { Grid, Paper, Button } from "material-ui";

export interface StyledProps {
    className?: string;
}

export function fullHeight<T extends React.ComponentType>(Component: T) {

    const FullHeightStyled = styled(Component as React.ComponentClass /* shut up the type checker */)`
        height: 100%
    `;

    return class extends React.Component<StyledProps> {
        render() {
            return <FullHeightStyled {...this.props} />;
        }
    } as T;
}

export const FullHeightGrid = fullHeight(Grid);

export interface CssGridProps {
    gridTemplate?: string;
}

export const CssGrid = styled.div`
    display: grid;
    grid-template: ${(props: CssGridProps) => props.gridTemplate ? props.gridTemplate : ""}
` as React.ComponentType<CssGridProps>;

export interface CssGridItemProps {
    gridArea?: string;
    justify?: "start" | "end" | "center" | "stretch";
    align?: "start" | "end" | "center" | "stretch";
}

export const CssGridItem = styled.div`
    grid-area: ${(props: CssGridItemProps) => props.gridArea ? props.gridArea : ""};
    justify-self: ${(props: CssGridItemProps) => props.justify ? props.justify : ""};
    align-self: ${(props: CssGridItemProps) => props.align ? props.align : ""};
`;

export const ButtonsArea = styled(CssGridItem)`
    margin-top: 16px;
`;

export const OffsetButton = styled(Button as any)`
    margin-top: 16px;
`;

export const EsteamationPaper = styled(Paper as any)`
    padding: 16px
`;
