export enum Role {
    Watcher = "watcher",
    Estimator = "estimator"
}

export interface Member {
    readonly name: string;
    readonly role: Role;
}

export class Estimation {
    readonly name: string;
    readonly values: { [name: string]: number};
    readonly isFinished: boolean;

    constructor(name: string) {
        this.name = name;
        this.values = {};
    }
}

export interface Team {
    readonly id: string;
    readonly estimations: Estimation[];
    readonly members: Member[];
}
