import * as React from "react";
import * as router from "react-router";

interface RouteParams {
    teamName: string;
}

export interface WithTeamNameProps {
    match: router.match<RouteParams>;
    teamName: string;
}

export function withTeamName<TOriginalProps extends {}>(
    Component: React.ComponentType<TOriginalProps & WithTeamNameProps>
) {
    type Props = TOriginalProps & WithTeamNameProps;
    return class extends React.Component<Props> {
        render() {
            return <Component {...this.props} teamName={this.props.match.params.teamName} />;
        }
    };
}
