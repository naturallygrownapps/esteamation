import * as React from "react";
import styled from "styled-components";
import { Typography, Button, TextField, FormControlLabel } from "material-ui";
import { CssGrid, CssGridItem, ButtonsArea } from "../util/layout";
import * as router from "react-router";
import SwitchElement from "material-ui/Switch/Switch";
import RadioGroup from "material-ui/Radio/RadioGroup";
import Radio from "material-ui/Radio/Radio";
import { Role, Member } from "../Entities";
import { EsteamationClient } from "./teamUtils";
import { withLoader, WithLoaderProps } from "../util/loader";
import { Redirect } from "react-router";
import { WithTeamNameProps, withTeamName } from "./teamComponents";

interface Props extends WithLoaderProps, WithTeamNameProps {
    onConnect: (c: EsteamationClient, m: Member) => any;
}

interface State {
    username: string;
    role: Role;
}

export default class JoinTeam extends React.Component<Props, State> {
    state: State = {
        username: "",
        role: Role.Estimator
    };

    usernameChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newUsername = e.target.value;
        this.setState({
            username: newUsername
        });
    }

    roleChanged = (event: object, value: string) => {
        this.setState({
            role: value as Role
        });
    }

    onSubmit = async (e: React.SyntheticEvent<any>) => {
        e.preventDefault();
        const client  = new EsteamationClient(this.props.teamName, this.state.username, this.state.role);
        await this.props.loadWhilePromise(client.connect());
        this.props.onConnect(client, { name: this.state.username, role: this.state.role });
    }

    render() {
        return (
            <form onSubmit={this.onSubmit}>
                <CssGrid>
                    <CssGridItem justify="center">
                        <Typography type="headline">Join team {this.props.teamName}</Typography>
                        <TextField autoFocus fullWidth label="Your Name" required onChange={this.usernameChanged} />
                        <RadioGroup value={this.state.role} onChange={this.roleChanged}>
                            <FormControlLabel value={Role.Estimator} control={<Radio />} label="I will estimate" />
                            <FormControlLabel value={Role.Watcher} control={<Radio />} label="I want to watch only" />
                        </RadioGroup>
                    </CssGridItem>
                    <ButtonsArea justify="center" >
                        <Button type="submit" raised color="primary">Join</Button>
                    </ButtonsArea>
                </CssGrid>
            </form>
        );
    }
}
