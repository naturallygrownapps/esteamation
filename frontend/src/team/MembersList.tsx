import * as React from "react";
import Typography from "material-ui/Typography/Typography";
import { Team,  Member } from "../Entities";

interface Props {
    members: Member[];
}

class MembersList extends React.Component<Props> {

    render() {
        const memberItems = this.props.members.map(m => (
            <div key={m.name}>
                <Typography>{`${m.name} (${m.role})`}</Typography>
            </div>
        ));

        return (
            <React.Fragment>
                <Typography type="caption">Members on the team:</Typography>
                {memberItems}
            </React.Fragment>
        );
    }
}
export default MembersList;
