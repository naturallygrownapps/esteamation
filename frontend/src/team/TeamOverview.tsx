import * as React from "react";
import { WithLoaderProps } from "../util/loader";
import * as router from "react-router";
import { WithTeamNameProps, withTeamName } from "./teamComponents";
import { EsteamationClient, clearLoginForTeam } from "./teamUtils";
import Typography from "material-ui/Typography/Typography";
import { Member, Team, Estimation } from "../Entities";
import { CssGrid, CssGridItem, OffsetButton } from "../util/layout";
import MembersList from "./MembersList";
import EstimationsList from "../estimation/EstimationsList";
import { Button, Modal, Divider } from "material-ui";
import styled from "styled-components";
import NewEstimationModal from "../estimation/NewEstimationModal";
import { Redirect } from "react-router";
import DeleteAllEstimationsDialog from "../estimation/DeleteAllEstimationsDialog";

const MembersListDivider = styled(Divider as any)`
`;
const ContentPanel = styled(CssGridItem)`
    width: 100%;
`;

interface Props extends WithLoaderProps, WithTeamNameProps {
    client: EsteamationClient;
    member: Member;
}

interface State {
    team?: Team;
    isAddingEstimation: boolean;
    isConfirmDeleteAllDialogOpen: boolean;
    loginCleared: boolean;
}

class TeamOverview extends React.Component<Props, State> {
    state: State = {
        isAddingEstimation: false,
        isConfirmDeleteAllDialogOpen: false,
        loginCleared: false
    };

    get client() { return this.props.client; }

    async componentDidMount() {
        const team = await this.props.loadWhilePromise(this.client.getTeam());
        this.setState({
            team
        });
        this.client.on("teamUpdated", this.onTeamUpdatedRemotely);
        this.client.on("estimationUpdated", this.onEstimationUpdatedRemotely);
    }

    async componentWillUnmount() {
        this.client.off("teamUpdated", this.onTeamUpdatedRemotely);
        this.client.off("estimationUpdated", this.onEstimationUpdatedRemotely);
    }

    onTeamUpdatedRemotely = (team: Team) => {
        this.setState({
            team
        });
    }

    onEstimationUpdatedRemotely = (estimation: Estimation) => {
        const currentTeam = this.state.team;
        if (!currentTeam) {
            console.error("Received new estimation, but have no team.");
            return;
        }

        const oldEstimations = currentTeam.estimations;
        const i = oldEstimations.findIndex(e => e.name === estimation.name);
        let newEstimations: Estimation[];
        if (i === -1) {
            newEstimations = oldEstimations.concat(estimation);
        } else {
            newEstimations = oldEstimations.slice(0);
            newEstimations.splice(i, 1, estimation);
        }

        this.setState({
            team: {
                ...currentTeam,
                estimations: newEstimations
            }
        });
    }

    onAddEstimationModalClosing = (estimation: Estimation) => {
        this.setState({
            isAddingEstimation: false
        });
        if (estimation) {
            this.props.loadWhilePromise(this.client.addEstimation(estimation).then(() => {
                // We will receive an update shortly, but we short circuit the update here
                // in case push notifications are delayed.
                this.onEstimationUpdatedRemotely(estimation);
            }));
        }
    }

    onEstimationValueSelected = (estimation: Estimation, value: number) => {
        this.props.loadWhilePromise(this.client.estimate(estimation, value).then(() => {
            const newValues = Object.assign({}, estimation.values);
            newValues[this.props.member.name] = value;
            this.onEstimationUpdatedRemotely({
                ...estimation,
                values: newValues
            });
        }));
    }

    onDeleteEstimationRequested = (estimation: Estimation) => {
        if (estimation) {
            this.props.loadWhilePromise(this.client.deleteEstimation(estimation).then(() => {
                const oldTeam = this.state.team;
                if (!oldTeam) {
                    return;
                }
                this.onTeamUpdatedRemotely({
                    ...oldTeam,
                    estimations: oldTeam.estimations.filter(e => e.name !== estimation.name)
                });
            }));
        }
    }

    onDeleteAllDialogClosed = (confirmed: boolean) => {
        this.setState({
            isConfirmDeleteAllDialogOpen: false
        });
        if (confirmed) {
            this.props.loadWhilePromise(this.client.deleteAllEstimations().then(() => {
                const oldTeam = this.state.team;
                if (!oldTeam) {
                    return;
                }
                this.onTeamUpdatedRemotely({
                    ...oldTeam,
                    estimations: []
                });
            }));
        }
    }

    onLeaveTeamClick = () => {
        this.client.disconnect();
        clearLoginForTeam(this.props.teamName);
        this.setState({
            loginCleared: true
        });
    }

    onDeleteAllClick = () => {
        this.setState({
            isConfirmDeleteAllDialogOpen: true
        });
    }

    render() {
        const team = this.state.team;
        if (this.state.loginCleared) {
            return <Redirect to="/" />;
        }

        if (!team) {
            return "";
        }

        return (
            <React.Fragment>
                <CssGrid gridTemplate={`
                    'teamOverviewHeader' auto
                    'teamOverviewContent' 1fr
                    / 1fr`}>
                    <CssGridItem gridArea="teamOverviewHeader" justify="center" >
                        <Typography type="subheading">
                            Hi {this.props.member.name}!
                            Welcome to team {this.props.teamName}
                        </Typography>
                    </CssGridItem>
                    <ContentPanel gridArea="teamOverviewContent" justify="start">
                        <EstimationsList
                            estimations={team.estimations}
                            onEstimationValueSelected={this.onEstimationValueSelected}
                            onDeleteEstimationRequested={this.onDeleteEstimationRequested}
                            user={this.props.member}
                            team={team}
                        />
                        <OffsetButton
                            raised
                            color="primary"
                            onClick={() => this.setState({ isAddingEstimation: true})}>
                            Start new estimation
                        </OffsetButton>
                        <OffsetButton
                            raised
                            style={{display: "block"}}
                            onClick={this.onDeleteAllClick}>
                            Delete all
                        </OffsetButton>

                        <MembersListDivider style={{margin: "15px 0"}} />

                        <MembersList members={team.members} />

                        <OffsetButton raised color="primary" onClick={this.onLeaveTeamClick}>Leave team</OffsetButton>
                    </ContentPanel>
                </CssGrid>
                <NewEstimationModal
                    isOpen={this.state.isAddingEstimation}
                    onCloseRequested={this.onAddEstimationModalClosing}
                />
                <DeleteAllEstimationsDialog
                    onClose={this.onDeleteAllDialogClosed}
                    isOpen={this.state.isConfirmDeleteAllDialogOpen}
                />
            </React.Fragment>
        );
    }
}
export default withTeamName(TeamOverview);
