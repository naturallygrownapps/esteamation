import * as React from "react";
import * as router from "react-router";
import { WithTeamNameProps, withTeamName } from "./teamComponents";
import { EsteamationClient, storeLoginForTeam, getLoginForTeam } from "./teamUtils";
import TeamOverview from "./TeamOverview";
import JoinTeam from "./JoinTeam";
import { withLoader, WithLoaderProps } from "../util/loader";
import { Member } from "../Entities";

interface Props extends WithTeamNameProps, WithLoaderProps {
}

interface State {
    client?: EsteamationClient;
    member?: Member;
}

class TeamRootPage extends React.Component<Props, State> {
    state: State = {
    };

    async componentDidMount() {
        const existingLogin = getLoginForTeam(this.props.teamName);
        if (existingLogin) {
            this.state.member = existingLogin;
            const client = new EsteamationClient(this.props.teamName, existingLogin.name, existingLogin.role);
            try {
                await this.props.loadWhilePromise(client.connect());
                this.setState({
                    client
                });
            } catch {
                this.setState({
                    client: undefined,
                    member: undefined
                });
            }
        }
    }

    onJoinedTeam = (c: EsteamationClient, m: Member) => {
        storeLoginForTeam(this.props.teamName, m);
        this.setState({
            client: c,
            member: m
        });
    }

    render() {
        return (
            this.state.client && this.state.member ?
                <TeamOverview {...this.props} client={this.state.client} member={this.state.member} /> :
                    <JoinTeam {...this.props} onConnect={this.onJoinedTeam} />
        );
    }
}
export default withLoader(withTeamName(TeamRootPage));
