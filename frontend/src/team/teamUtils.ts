import { Role, Member, Team, Estimation } from "../Entities";
import * as io from "socket.io-client";
import { ServerAddress } from "../util/constants";
import * as React from "react";
import axios, { AxiosPromise } from "axios";

export class EsteamationClient {
    private client: SocketIOClient.Socket|null;

    constructor(private teamName: string, private username: string, private role: Role) {

    }

    private getApiUrl(url: string) {
        const teamEncoded = encodeURIComponent(this.teamName);
        return `${ServerAddress}/api/team/${teamEncoded}/${url}`;
    }

    private get(url: string) {
        return axios.get(this.getApiUrl(url));
    }

    private post<T>(url: string, data: any) {
        return axios.post<T>(this.getApiUrl(url), data);
    }

    private delete(url: string) {
        return axios.delete(this.getApiUrl(url));
    }

    async getTeam() {
        const response = await this.get("");
        return response.data as Team;
    }

    addEstimation(estimation: Estimation) {
        return this.post<any>("estimation", estimation);
    }

    deleteEstimation(estimation: Estimation) {
        const estimationName = encodeURIComponent(estimation.name);
        return this.delete(`estimation/${estimationName}`);
    }

    deleteAllEstimations() {
        return this.delete(`estimation?type=all`);
    }

    estimate(estimation: Estimation, value: number) {
        const estimationName = encodeURIComponent(estimation.name);
        return this.post(`estimation/${estimationName}`, {
            estimator: this.username,
            value
        });
    }

    disconnect() {
        if (this.client) {
            this.client.removeAllListeners();
            this.client.close();
            this.client = null;
        }
    }

    connect() {
        this.disconnect();
        const c = io.connect(ServerAddress);
        this.client = c;

        return new Promise((res, rej) => {
            const unregisterErrorHandlers = EsteamationClient.registerOnceForErrors(c, e => {
                c.off("joined");
                c.off("connect");
                rej(e);
            });

            c.once("joined", () => {
                res("joined");
            });
            // Use "on" for automatic reconnect.
            c.on("connect", async () => {
                unregisterErrorHandlers();
                c.emit("joinTeam", {
                    teamId: this.teamName,
                    member: {
                        name: this.username,
                        role: this.role
                    }
                });
            });
        });
    }

    on(event: string, callback: Function) {
        if (!this.client) {
            throw new Error("not connected");
        }

        this.client.on(event, callback);
    }

    off(event: string, callback: Function) {
        if (this.client) {
            this.client.off(event, callback);
        }
    }

    private static registerOnceForErrors(c: SocketIOClient.Socket, onError: (e: Error) => any) {
        const errorEvents = [
            "error",
            "connect_error",
            "connect_timeout"
        ];

        const unregisterEvents = () => errorEvents.forEach(evtName => c.off(evtName, errorHandlers[evtName]));

        const errorHandlers = errorEvents.reduce((result, errorEvent) => {
            result[errorEvent] = (e: Error) => {
                // unregister all error events on first error
                unregisterEvents();
                onError(e);
            };
            return result;
        // tslint:disable-next-line:align
        }, {});

        errorEvents.forEach(evtName => c.on(evtName, errorHandlers[evtName]));
        return unregisterEvents;
    }
}

const STORED_LOGINS_KEY = "logins";
let storedLogins: { [teamName: string]: Member }|null = null;
function getStoredLogins() {
    if (!storedLogins) {
        const serialized = localStorage.getItem(STORED_LOGINS_KEY);
        if (serialized) {
            try {
                storedLogins = JSON.parse(serialized);
            } catch (e) {
                localStorage.setItem(STORED_LOGINS_KEY, "");
            }
        }
        if (!storedLogins) {
            storedLogins = {};
        }
    }
    return storedLogins;
}
export function storeLoginForTeam(teamName: string, member: Member) {
    getStoredLogins()[teamName] = member;
    localStorage.setItem(STORED_LOGINS_KEY, JSON.stringify(storedLogins));
}
export function clearLoginForTeam(teamName: string) {
    const logins = getStoredLogins();
    delete logins[teamName];
    localStorage.setItem(STORED_LOGINS_KEY, JSON.stringify(storedLogins));
}
export function getLoginForTeam(teamName: string) {
    return getStoredLogins()[teamName];
}
