import * as React from "react";
import "./App.css";
import styled from "styled-components";
import { Grid, Toolbar, Typography } from "material-ui";
import AppBar from "material-ui/AppBar/AppBar";
import * as c from "../util/constants";
import { FullHeightGrid, CssGrid, CssGridItem } from "../util/layout";
import Title from "./Title";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import createMuiTheme from "material-ui/styles/createMuiTheme";
import { indigo, lightGreen, red, blue, cyan } from "material-ui/colors";
import { AppContent } from "./AppContent";

const theme = createMuiTheme({
    palette: {
        primary: blue,
        secondary: cyan,
        error: red,
    }
});

class App extends React.Component {
  render() {
    return (
        <MuiThemeProvider theme={theme}>
            <CssGrid gridTemplate={"'header' auto 'content' 1fr / 1fr"}>
                <CssGridItem gridArea="header">
                    <AppBar position="static">
                        <Toolbar>
                            <Title />
                        </Toolbar>
                    </AppBar>
                </CssGridItem>
                <CssGridItem gridArea="content">
                    <AppContent />
                </CssGridItem>
            </CssGrid>
        </MuiThemeProvider>
    );
  }
}

export default App;
