import * as React from "react";
import SelectTeam from "../landing/SelectTeam";
import { Route, Switch } from "react-router";
import { EsteamationPaper } from "../util/layout";
import TeamRootPage from "../team/TeamRootPage";

export class AppContent extends React.Component {
    render() {

        return (
            <EsteamationPaper>
                <Switch>
                    <Route path="/" exact component={SelectTeam} />
                    <Route path="/:teamName" exact component={TeamRootPage} />
                </Switch>
            </EsteamationPaper>
        );
    }
}
