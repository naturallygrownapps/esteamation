import * as React from "react";
import * as ReactDOM from "react-dom";
import * as enzyme from "enzyme";
import App from "./App";
import { Button } from "material-ui";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<App />, div);
});

// it("renders a button", () => {
//     const app = enzyme.shallow(<App />);
//     expect(app.find(Button)).toHaveLength(1);
// });
