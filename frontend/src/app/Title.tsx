import * as React from "react";
import Typography from "material-ui/Typography/Typography";

export default function Title() {
    return (
        <React.Fragment>
            <Typography type="title" color="inherit">es</Typography>
            <Typography type="title" color="inherit">TEAM</Typography>
            <Typography type="title" color="inherit">ation</Typography>
        </React.Fragment>
    );
}
