FROM node:8.9-alpine
LABEL maintainer="Rufus Linke <mail@rufuslinke.de>"

WORKDIR /esteamation

COPY package.json yarn.lock ./
COPY api/package.json api/yarn.lock ./api/
COPY frontend/package.json frontend/yarn.lock ./frontend/
RUN yarn install

COPY . .

RUN yarn build

EXPOSE 3000
CMD [ "yarn", "start" ]
