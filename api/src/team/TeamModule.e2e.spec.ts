import { plainToClass, plainToClassFromExist } from "class-transformer";
import * as express from "express";
import * as request from "supertest";
import { Test } from "@nestjs/testing";
import { TeamModule } from "./TeamModule";
import { Team } from "./Team";
import { Estimation, EstimationValue } from "./Estimation";
import { HttpStatus } from "@nestjs/common";
import { Member, Role } from "./Member";
import "../common/setvars";
import * as io from "socket.io-client";
import { JoinTeamMessage } from "./TeamGateway";
import { INestApplication } from "@nestjs/common/interfaces";
import { Server } from "http";
import { setTimeout } from "timers";
import { HttpExceptionFilter } from "../common/HttpExceptionFilter";
import { Response } from "express-serve-static-core";
import { isDebugging } from "../common/utils";

if (isDebugging()) {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 10 * 60 * 1000;
}

describe("TeamModule", () => {
    let expressApp: express.Express;
    let app: INestApplication;
    let port: number;

    const teamId = "myteam";

    async function getBody<T>(ctor: {new (...args: any[]): T}, url: string) {
        const res = await request(expressApp)
            .get(url)
            .expect(HttpStatus.OK);
        return plainToClass<T, object>(ctor, res.body);
    }

    function wsAddress() {
        const address = `ws://localhost:${port}`;
        return address;
    }

    beforeEach(async () => {
        expressApp = express();

        const module = await Test.createTestingModule({
            modules: [TeamModule],
          })
          .compile();

        app = module.createNestApplication(expressApp);
        const server: Server = await app.listen(0);
        port = server.address().port;
    });

    it("GETs a team", async () => {
        const team = await getBody(Team, `/team/${teamId}`);
        expect(team.id).toBe(teamId);
    });

    it("handles teams with URL encoding", async () => {
        const specialTeamId = "team/name&more";
        const team = await getBody(Team, `/team/${encodeURIComponent(specialTeamId)}`);
        expect(team.id).toBe(specialTeamId);
    });

    it("POSTs a new estimation", async () => {
        const name = "myestimation";
        await request(expressApp)
            .post(`/team/${teamId}/estimation`)
            .send(new Estimation(name))
            .expect(HttpStatus.CREATED);
        const es = await getBody(Estimation, `/team/${teamId}/estimation/${name}`);
        expect(es.name).toBe(name);
    });

    it("handles estimations with URL encoding", async () => {
        const name = "my/estim&ation";
        await request(expressApp)
            .post(`/team/${teamId}/estimation`)
            .send(new Estimation(name))
            .expect(HttpStatus.CREATED);
        const es = await getBody(Estimation, `/team/${teamId}/estimation/${encodeURIComponent(name)}`);
        expect(es.name).toBe(name);
    });

    it("handles missing estimations on GET", () => {
        return request(expressApp)
            .get(`/team/${teamId}/estimation/randomname`)
            .expect(HttpStatus.NOT_FOUND);
    });

    it("cannot POST an existing estimation", async () => {
        const name = "myestimation";
        await request(expressApp)
            .post(`/team/${teamId}/estimation`)
            .send(new Estimation(name))
            .expect(HttpStatus.CREATED);

        await request(expressApp)
            .post(`/team/${teamId}/estimation`)
            .send(new Estimation(name))
            .expect(HttpStatus.CONFLICT);
    });

    it("connects a member on 'joinTeam' message", () => {
        return new Promise((resolve, reject) => {
            const client = io.connect(wsAddress());
            const member = new Member("me", Role.Watcher);

            client.on("teamUpdated", (team: Team) => {
                expect(team.id).toBe(teamId);
                expect(team.members).toContainEqual(jasmine.objectContaining(member));
                resolve();
            });
            client.once("connect", async () => {
                client.emit("joinTeam", new JoinTeamMessage(teamId, member));
            });
            client.on("exception", reject);
        });
    });

    it("removes a member when it disconnects", () => {
        return new Promise((resolve, reject) => {
            const client = io.connect(wsAddress());
            const member = new Member("me", Role.Watcher);

            client.once("connect", async () => {
                client.emit("joinTeam", new JoinTeamMessage(teamId, member));
            });

            client.on("joined", async () => {
                const team = await getBody(Team, `/team/${teamId}`);
                expect(team.members).toContainEqual(jasmine.objectContaining(member));
                client.disconnect();
            });

            client.once("disconnect", () => {
                // Wait until server processed disconnect. Ugly and error-prone, but easy.
                setTimeout(async () => {
                    const team = await getBody(Team, `/team/${teamId}`);
                    expect(team.members).not.toContainEqual(jasmine.objectContaining(member));
                    resolve();
                }, 500);
            });

            client.on("exception", reject);
        });
    });

    it("receives a message when someone estimates", () => {
        return new Promise(async (resolve, reject) => {
            const member1 = new Member("me", Role.Watcher);
            const member2 = new Member("estimator", Role.Estimator);
            const estimationName = "testEstimation";
            const estimationValue = 2;

            const client1 = io.connect(wsAddress());
            let updateEventCount = 0;
            client1.on("estimationUpdated", (estimation: Estimation) => {
                // First event sends the new estimation, second sends the estimated value.
                updateEventCount++;
                if (updateEventCount === 2) {
                    const value = (estimation.values as any)[member2.name];
                    expect(value).toBe(estimationValue);
                    resolve();
                }
            });
            client1.once("connect", async () => {
                client1.emit("joinTeam", new JoinTeamMessage(teamId, member1));
            });
            client1.on("exception", reject);

            const client2 = io.connect(wsAddress());
            client2.once("connect", async () => {
                client2.emit("joinTeam", new JoinTeamMessage(teamId, member2));
            });
            client2.once("joined", async () => {
                await request(expressApp)
                    .post(`/team/${teamId}/estimation`)
                    .send(new Estimation(estimationName))
                    .expect(HttpStatus.CREATED);
                await request(expressApp)
                    .post(`/team/${teamId}/estimation/${estimationName}`)
                    .send(new EstimationValue(member2.name, estimationValue))
                    .expect(HttpStatus.CREATED);
            });
            client2.on("exception", reject);
        });
    });

    it("can join a team with an existing member name", () => {
        return new Promise((resolve, reject) => {
            const client = io.connect(wsAddress());
            const member = new Member("me", Role.Watcher);

            // Joins with the same name.
            function connectClient2() {
                const client2 = io.connect(wsAddress());
                client2.on("exception", (e: any) => console.log(e));
                client2.once("connect", () => {
                    client2.emit("joinTeam", new JoinTeamMessage(teamId, member));
                });
                client2.once("joined", () => {
                    resolve();
                });
                client2.on("exception", reject);
            }

            client.once("joined", () => {
                connectClient2();
            });
            client.once("connect", async () => {
                client.emit("joinTeam", new JoinTeamMessage(teamId, member));
            });
            client.on("exception", reject);
        });
    });
});
