import { Estimation } from "./Estimation";
import { Member } from "./Member";

export class Team {
    readonly estimations: Estimation[] = [];
    readonly members: Member[] = [];

    constructor(public readonly id: string) {
    }
}
