import { Team } from "./Team";
import { Component } from "@nestjs/common";
import { Estimation } from "./Estimation";
import { Member, MemberName, Role } from "./Member";

@Component()
export class EstimationService {

    static checkEstimationFinished(team: Team, estimation: Estimation) {
        return team.members
            .filter(m => m.role === Role.Estimator)
            .every(m => typeof estimation.values[m.name] !== "undefined");
    }

    addEstimatedValue(team: Team, estimation: Estimation, memberName: MemberName, value: number) {
        estimation.values[memberName] = value;
        estimation.isFinished = EstimationService.checkEstimationFinished(team, estimation);
    }

}
