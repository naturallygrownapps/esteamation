import {
    WebSocketGateway,
    WebSocketServer,
    SubscribeMessage,
    WsResponse,
    OnGatewayConnection,
    OnGatewayDisconnect
} from "@nestjs/websockets";
import { UsePipes, ValidationPipe } from "@nestjs/common";
import { Member } from "./Member";
import { validate, IsDefined } from "class-validator";
import { plainToClass } from "class-transformer";
import { TeamService } from "./TeamService";
import { Team } from "./Team";
import { Estimation } from "./Estimation";

export class JoinTeamMessage {
    @IsDefined()
    readonly teamId: string;
    @IsDefined()
    readonly member: Member;

    constructor(teamId: string, member: Member) {
        this.teamId = teamId;
        this.member = member;
    }
}

type ClientId = string;
type TeamId = string;

@WebSocketGateway()
export class TeamGateway implements OnGatewayDisconnect {
    @WebSocketServer()
    private server: SocketIO.Server;
    private connectedMembers: Map<ClientId, TeamId>;

    constructor(private teamService: TeamService) {
        this.connectedMembers = new Map();
    }

    handleDisconnect(client: SocketIO.Socket) {
        const clientId = client.id;
        const teamId = this.connectedMembers.get(clientId);
        if (teamId) {
            this.connectedMembers.delete(clientId);
            const team = this.teamService.getTeam(teamId);
            this.teamService.deleteMemberByConnectionId(team, clientId);
            this.notifyTeamUpdated(team);
        }
    }

    @SubscribeMessage("joinTeam")
    @UsePipes(new ValidationPipe())
    async onEvent(client: SocketIO.Socket, data: any) {
        const joinTeamMsg = plainToClass<JoinTeamMessage, object>(JoinTeamMessage, data);
        const errors = await validate(joinTeamMsg);
        if (errors.length) {
            client.emit("exception", {
                error: errors.join("\n")
            });
            client.disconnect(true);
        }
        else {
            const teamId = joinTeamMsg.teamId;
            const member = joinTeamMsg.member;

            client.join(teamId.toLowerCase());

            this.connectedMembers.set(client.id, teamId);
            member.connectionId = client.id;
            const team = this.teamService.getTeam(teamId);
            this.teamService.addOrUpdateMember(team, member);
            this.notifyTeamUpdated(team);
            return {
                event: "joined"
            };
        }
    }

    notifyTeamUpdated(team: Team) {
        this.server.to(team.id.toLowerCase()).emit("teamUpdated", team);
    }

    notifyEstimationUpdated(team: Team, estimation: Estimation) {
        this.server.to(team.id.toLowerCase()).emit("estimationUpdated", estimation);
    }

}
