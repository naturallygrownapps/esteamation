import { TeamService } from "./TeamService";
import { nameof } from "../common/utils";
import { Test } from "@nestjs/testing";

describe(TeamService.name, () => {
  let teamService: TeamService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
        components: [TeamService],
      }).compile();

    teamService = module.get<TeamService>(TeamService);
  });

  describe("get non-existing team", () => {
    it("should return a new team", async () => {
        const testId = "randomId";
        expect(teamService.teams.size).toBe(0);
        expect(teamService.getTeam(testId).id).toBe(testId);
        expect(teamService.teams.size).toBe(1);
    });
  });
});
