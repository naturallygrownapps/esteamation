import { IsDefined } from "class-validator";
import { Member, MemberName } from "./Member";

export class EstimationValue {
    @IsDefined()
    readonly estimator: MemberName;
    @IsDefined()
    readonly value: number;

    constructor(estimator: MemberName, value: number) {
        this.estimator = estimator;
        this.value = value;
    }
}

export class Estimation {
    @IsDefined()
    readonly name: string;
    readonly values: { [name: string]: number} = {};
    isFinished = false;

    constructor(name: string) {
        this.name = name;
    }
}
