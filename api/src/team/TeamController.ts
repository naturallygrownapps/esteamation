import {
    Controller,
    Get,
    Post,
    Param,
    NotFoundException,
    HttpStatus,
    Body,
    UsePipes,
    ValidationPipe,
    ForbiddenException,
    Delete,
    Query,
    BadRequestException
} from "@nestjs/common";
import { TeamService } from "./TeamService";
import { Estimation, EstimationValue } from "./Estimation";
import { Member, Role } from "./Member";
import { EstimationService } from "./EstimationService";
import { TeamGateway } from "./TeamGateway";

@Controller("team")
export class TeamController {
    constructor(
        private readonly teamService: TeamService,
        private readonly estimationService: EstimationService,
        private readonly teamGateway: TeamGateway) {

    }

    @Get(":id")
    getTeam(@Param("id") teamId: string) {
        return this.teamService.getTeam(teamId);
    }

    @Get(":teamId/estimation/:estimationName")
    getEstimation(@Param("teamId") teamId: string, @Param("estimationName") estimationName?: string) {
        const team = this.teamService.getTeam(teamId);
        const estimation = estimationName ? this.teamService.getEstimation(team, estimationName) : null;
        if (!estimation) {
            throw new NotFoundException("Estimation not found.");
        }

        return estimation;
    }

    @Delete(":teamId/estimation/:estimationName")
    deleteEstimation(@Param("teamId") teamId: string, @Param("estimationName") estimationName?: string) {
        const team = this.teamService.getTeam(teamId);
        const estimationIdx = team.estimations.findIndex(e => e.name === estimationName);
        if (estimationIdx === -1) {
            throw new NotFoundException("Estimation not found.");
        }
        team.estimations.splice(estimationIdx, 1);
        this.teamGateway.notifyTeamUpdated(team);
    }

    @Delete(":teamId/estimation")
    deleteByType(@Param("teamId") teamId: string, @Query("type") type: string) {
        if (type === "all") {
            const team = this.teamService.getTeam(teamId);
            team.estimations.length = 0;
            this.teamGateway.notifyTeamUpdated(team);
        }
        else {
            throw new BadRequestException(`Unknown type: ${type}`);
        }
    }

    @Post(":teamId/estimation")
    @UsePipes(new ValidationPipe())
    addEstimation(
        @Param("teamId") teamId: string,
        @Body() estimation: Estimation)
    {
        const team = this.teamService.getTeam(teamId);
        this.teamService.addEstimation(team, new Estimation(estimation.name));
        this.teamGateway.notifyEstimationUpdated(team, estimation);
    }

    @Post(":teamId/estimation/:estimationName")
    estimate(
        @Param("teamId") teamId: string,
        @Param("estimationName") estimationName: string,
        @Body() estimationValue: EstimationValue) {

        const team = this.teamService.getTeam(teamId);
        const estimation = this.teamService.getEstimation(team, estimationName);
        if (!estimation) {
            throw new NotFoundException(`Estimation "${estimationName}" not found.`);
        }

        const teamMember = this.teamService.getMember(team, estimationValue.estimator);
        if (!teamMember) {
            throw new NotFoundException(`Team member "${estimationValue.estimator}" not found`);
        }
        if (teamMember.role !== Role.Estimator) {
            throw new ForbiddenException("Only estimators can give estimates.");
        }

        this.estimationService.addEstimatedValue(team, estimation, estimationValue.estimator, estimationValue.value);

        this.teamGateway.notifyEstimationUpdated(team, estimation);
    }
}

