import { Component, ConflictException } from "@nestjs/common";
import { Team } from "./Team";
import { Estimation } from "./Estimation";
import { Member } from "./Member";
import { EstimationService } from "./EstimationService";

@Component()
export class TeamService {
    teams: Map<string, Team> = new Map();

    getTeam(id: string) {
        if (!id) {
            throw new Error("Missing team id.");
        }
        const lowerCaseId = id.toLowerCase();

        let team = this.teams.get(lowerCaseId);
        if (!team) {
            team = new Team(id);
            this.teams.set(lowerCaseId, team);
        }
        return team;
    }

    getEstimation(team: Team, estimationName: string) {
        return team.estimations.find(e => e.name === estimationName);
    }

    addEstimation(team: Team, estimation: Estimation) {
        const existing = this.getEstimation(team, estimation.name);
        if (existing)
            throw new ConflictException(`An estimation with name ${estimation.name} already exists.`);
        team.estimations.push(estimation);
    }

    getMember(team: Team, memberName: string) {
        return team.members.find(m => m.name === memberName);
    }

    addMember(team: Team, member: Member) {
        const existing = this.getMember(team, member.name);
        if (existing)
            throw new ConflictException(`A team member with name ${member.name} already exists.`);
        team.members.push(member);
    }

    addOrUpdateMember(team: Team, member: Member) {
        const existingMemberIdx = team.members.findIndex(m => m.name === member.name);
        if (existingMemberIdx > -1) {
            team.members.splice(existingMemberIdx, 1);
        }
        team.members.push(member);
    }

    deleteMemberByConnectionId(team: Team, connectionId: string) {
        const memberIdx = team.members.findIndex(m => m.connectionId === connectionId);
        if (memberIdx > -1) {
            team.members.splice(memberIdx, 1);
        }
    }
}
