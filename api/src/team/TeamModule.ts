import { Module } from "@nestjs/common";
import { TeamController } from "./TeamController";
import { TeamService } from "./TeamService";
import { EstimationService } from "./EstimationService";
import { TeamGateway } from "./TeamGateway";

@Module({
    modules: [],
    controllers: [TeamController],
    components: [TeamService, EstimationService, TeamGateway],
})
export class TeamModule {}
