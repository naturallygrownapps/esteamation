import { IsDefined } from "class-validator";

export type MemberName = string;

export enum Role {
    Watcher = "watcher",
    Estimator = "estimator"
}

export class Member {
    @IsDefined()
    readonly name: MemberName;
    @IsDefined()
    readonly role: Role;
    connectionId: string;

    constructor(name: MemberName, role: Role) {
        this.name = name;
        this.role = role;
    }
}
