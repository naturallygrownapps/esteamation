import { Module } from "@nestjs/common";
import { TeamModule } from "./team/TeamModule";


@Module({
    modules: [TeamModule]
})
export class RootModule {
}
