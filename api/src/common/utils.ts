export function nameof<T>(key: keyof T, instance?: T): keyof T {
    return key;
}

export function isDebugging() {
    const argv = process.execArgv.join();
    return argv.includes("inspect") || argv.includes("debug");
}
