import { Catch, HttpException, ExceptionFilter } from "@nestjs/common";
import { Response } from "express-serve-static-core";

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, response: Response) {
      console.error(exception);
  }
}
