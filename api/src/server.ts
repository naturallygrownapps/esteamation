import { NestFactory } from "@nestjs/core";
import { RootModule } from "./RootModule";
import "./common/setvars";
import * as express from "express";
import * as proxy from "http-proxy-middleware";
import * as historyFallback from "connect-history-api-fallback";

async function bootstrap() {
    const expressApp = express();
    expressApp.use(historyFallback());
    if (process.env.NODE_ENV === "production") {
        expressApp.use([/^\/static($|\/)/, "/"], express.static(`${__dirname}/client`));
        expressApp.use("/static", express.static(`${__dirname}/client/static`));
    }
    else {
        // Proxy to debug server.
        const p  = proxy(["/**", "!/api/**"], {
            target: "http://localhost:3001",
            router: {
                "localhost:3000" : "http://localhost:3001"
            }
        });
        expressApp.use("/", p);
    }
    const app = await NestFactory.create(RootModule, expressApp);
    app.setGlobalPrefix("api");
    await app.listen(3000);
}
bootstrap();
